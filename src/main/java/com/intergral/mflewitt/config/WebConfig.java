package com.intergral.mflewitt.config;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.intergral.mflewitt"})
public class WebConfig implements WebMvcConfigurer {

    @Autowired
    private ApplicationContext applicationContext;

    @Bean

    DataSource dataSource() {
    //use this for DB2
//        BasicDataSource basicDataSource = new BasicDataSource();
//        basicDataSource.setUrl( "jdbc:db2://localhost:50000/test" );
//        basicDataSource.setUsername( "db2inst1" );
//        basicDataSource.setPassword( "xxxxxxxxxxxxxxxx" );
//        basicDataSource.setDriverClassName( "com.ibm.db2.jcc.DB2Driver" );
//        return basicDataSource;

    //Use this to connect to MySQL
        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setUrl( "jdbc:mysql://127.0.0.1:3306/Application?useLegacyDatetimeCode=false&serverTimezone=America/New_York" );
        basicDataSource.setUsername( "root" );
        basicDataSource.setPassword( "root" );
        basicDataSource.setDriverClassName( "com.mysql.cj.jdbc.Driver" );
        return basicDataSource;

//Use this database to stop routing through DBCP2
//        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
//        driverManagerDataSource.setUrl("jdbc:mysql://127.0.0.1:3306/Application?useLegacyDatetimeCode=false&serverTimezone=America/New_York");
//        driverManagerDataSource.setUsername("root");
//        driverManagerDataSource.setPassword("root");
//        driverManagerDataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
//        return driverManagerDataSource;
    }

    @Bean
    public SpringResourceTemplateResolver templateResolver() {

        SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();

        templateResolver.setApplicationContext(applicationContext);
        templateResolver.setPrefix("classpath:/templates/");
        templateResolver.setSuffix(".html");

        return templateResolver;
    }

    @Bean
    public SpringTemplateEngine templateEngine() {

        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.setTemplateResolver(templateResolver());
        templateEngine.setEnableSpringELCompiler(true);

        return templateEngine;
    }

    @Bean
    public ViewResolver viewResolver() {

        ThymeleafViewResolver resolver = new ThymeleafViewResolver();
        ViewResolverRegistry registry = new ViewResolverRegistry(null, applicationContext);

        resolver.setTemplateEngine(templateEngine());
        registry.viewResolver(resolver);

        return resolver;
    }
}