package com.intergral.mflewitt.model;

import java.sql.Date;

public class TestTable
{
    private int id;
    private String string1;
    private String string2;
    private int num1;
    private double double1;


    public TestTable()
    {

    }


    public TestTable( int id, String string1, String string2, int num1, double double1 )
    {
        this.id = id;
        this.string1 = string1;
        this.string2 = string2;
        this.num1 = num1;
        this.double1 = double1;
    }


    public int getId()
    {
        return id;
    }


    public void setId( final int id )
    {
        this.id = id;
    }


    public String getString1()
    {
        return string1;
    }


    public void setString1( final String string1 )
    {
        this.string1 = string1;
    }


    public String getString2()
    {
        return string2;
    }


    public void setString2( final String string2 )
    {
        this.string2 = string2;
    }


    public int getNum1()
    {
        return num1;
    }


    public void setNum1( final int num1 )
    {
        this.num1 = num1;
    }


    public double getDouble1()
    {
        return double1;
    }


    public void setDouble1( final double double1 )
    {
        this.double1 = double1;
    }


    @Override
    public String toString()
    {
        return "ID:" + id;
    }
}
