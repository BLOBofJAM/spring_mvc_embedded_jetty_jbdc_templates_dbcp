package com.intergral.mflewitt.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class TestTableMapper implements RowMapper<TestTable>
{

    public TestTable mapRow( final ResultSet resultSet, final int i ) throws SQLException
    {
        TestTable test = new TestTable(  );
        test.setId( resultSet.getInt( "id" ) );
        test.setString1( resultSet.getString( "String1" ) );
        test.setString2( resultSet.getString( "String2" ) );
        test.setNum1( resultSet.getInt( "Num1" ) );
        test.setDouble1( resultSet.getDouble( "Double1" ) );
        return test;
    }
}